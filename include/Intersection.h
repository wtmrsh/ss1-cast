#pragma once

// Platform
#include "Platform.h"

// User headers
#include "Vector2.h"

class Intersection
{
public:

	static bool rayLine(Vector2 const& origin, Vector2 const& dir, Vector2 const& line0, Vector2 const& line1, Vector2* hitPos = nullptr, float* hitDist = nullptr);
};