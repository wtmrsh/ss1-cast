#pragma once

// Platform
#include "Platform.h"

// User headers
#include "Vector2.h"

class Actor
{
	Vector2 mPosition;

	float mAngle;

public:

	Actor();

	virtual ~Actor() = default;

	Vector2 const& getPosition() const;

	float getAngle() const;

	Vector2 getDirection() const;

	void move(float distance);

	void turn(float angle);
};
