#pragma once

// Platform settings - based off OGRE3D (www.ogre3d.org)
#define SR_PLATFORM_WIN32 1
#define SR_PLATFORM_LINUX 2
#define SR_PLATFORM_APPLE 3

#define SR_COMPILER_MSVC 1
#define SR_COMPILER_GNUC 2
#define SR_COMPILER_BORL 3

// Find compiler information
#if defined( _MSC_VER )
#   define SR_COMPILER SR_COMPILER_MSVC
#   define SR_COMP_VER _MSC_VER
#elif defined( __GNUC__ )
#   define SR_COMPILER SR_COMPILER_GNUC
#   define SR_COMP_VER (((__GNUC__)*100) + \
	(__GNUC_MINOR__ * 10) + \
	__GNUC_PATCHLEVEL__)
#elif defined( __BORLANDC__ )
#   define SR_COMPILER SR_COMPILER_BORL
#   define SR_COMP_VER __BCPLUSPLUS__
#else
#   pragma error "Unknown compiler."
#endif

// Set platform
#if defined( __WIN32__ ) || defined( _WIN32 )
#   define SR_PLATFORM SR_PLATFORM_WIN32
#elif defined( __APPLE_CC__)
#   define SR_PLATFORM SR_PLATFORM_APPLE
#else
#   define SR_PLATFORM SR_PLATFORM_LINUX
#endif

// Ok, because only occurs on non-public STL members
#if SR_PLATFORM == SR_PLATFORM_WIN32
#	pragma warning(disable: 4251)
#endif
