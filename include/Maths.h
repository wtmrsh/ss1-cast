#pragma once

// Platform
#include "Platform.h"

#define SR_PI				3.14159265f
#define SR_TWOPI			(2 * SR_PI)
#define SR_DEGTORAD(d)		(d * SR_PI / 180.0f)
#define SR_RADTODEG(r)		(r * 180.0f / SR_PI)

enum Winding
{
	Clockwise,
	Anticlockwise
};
