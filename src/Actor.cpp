#include "Actor.h"

Actor::Actor()
	: mPosition()
	, mAngle(0.0f)
{
}

Vector2 const& Actor::getPosition() const
{
	return mPosition;
}

float Actor::getAngle() const
{
	return mAngle;
}

Vector2 Actor::getDirection() const
{
	return Vector2::fromAngle(getAngle());
}

void Actor::move(float distance)
{
	mPosition += getDirection() * distance;
}

void Actor::turn(float angle)
{
	mAngle += angle;
}