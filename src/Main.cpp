// Platform
#include "Platform.h"

// Standard headers
#include <iostream>

#if SR_PLATFORM == SR_PLATFORM_WIN32
#   include <windows.h>
#endif

// User headers
#include "SDL.h"
#include "Actor.h"
#include "Vector2.h"
#include "Intersection.h"

#define COLOUR3(r, g, b) (((b) << 24) + ((g) << 16) + ((r) << 8) + 255)
#define COLOUR4(r, g, b, a) (((b) << 24) + ((g) << 16) + ((r) << 8) + (a))

using namespace std;

static int gScreenWidth = 640;
static int gScreenHeight = 480;

Actor gPlayer;

void render(int* pixels, int stride, int x, int y, int width, int height)
{
	// Test wall
	Vector2 v0(-60, 100), v1(60, 100);
	float wallHeight = 60;

	// Viewport
	int x1 = x + width;
	int y1 = y + height;
	int xCentre = x + width / 2;
	int yCentre = y + height / 2;

	// View setup
	Vector2 const& viewOrigin = gPlayer.getPosition();
	float viewAngle = gPlayer.getAngle();

	// Larger FOV creates spherical distortion
	float viewFov = 60.0f;
	float distToViewPlane = width / tan(SR_DEGTORAD(viewFov / 2.0f));

	float fAngle = -viewFov / 2.0f;
	float dAngle = viewFov / gScreenWidth;

	for (int xp = x; xp < x1; ++xp)
	{
		Vector2 viewDir = Vector2::fromAngle(viewAngle + fAngle);

		Vector2 hitPos;
		float hitDist;

		if (Intersection::rayLine(viewOrigin, viewDir, v0, v1, &hitPos, &hitDist))
		{
			// Fisheye correction
			hitDist *= cos(SR_DEGTORAD(fAngle));

			int h = wallHeight * distToViewPlane / hitDist;
			int yStart = max(y, yCentre - h / 2);
			int yEnd = min(y1, yStart + h);

			int offset = gScreenWidth * yStart + xp;
			for (int yp = yStart; yp < yEnd; ++yp)
			{
				pixels[offset] = COLOUR3(128, 128, 128);
				offset += stride;
			}
		}

		fAngle += dAngle;
	}
}

static bool moveForward = false;
static bool moveBackward = false;
static bool turnLeft = false;
static bool turnRight = false;

void keyboardHandler(KeyEvent keyEvent, Key key, KeyModifiers keyModifiers)
{
	if (keyEvent == KeyEvent::Pressed)
	{
		switch (key)
		{
		case Key::Escape:
			throw exception("Exiting");

		case Key::UpArrow:
			moveForward = true;
			break;

		case Key::DownArrow:
			moveBackward = true;
			break;

		case Key::LeftArrow:
			turnLeft = true;
			break;

		case Key::RightArrow:
			turnRight = true;
			break;
		}
	}
	else if (keyEvent == KeyEvent::Released)
	{
		switch (key)
		{
		case Key::UpArrow:
			moveForward = false;
			break;

		case Key::DownArrow:
			moveBackward = false;
			break;

		case Key::LeftArrow:
			turnLeft = false;
			break;

		case Key::RightArrow:
			turnRight = false;
			break;
		}
	}
}

//
// Entry point
//
#if SR_PLATFORM == SR_PLATFORM_WIN32
int WINAPI WinMain(
	HINSTANCE hInstance,
	HINSTANCE hPrevInstance,
	LPSTR lpCmdLine,
	int nCmdShow)
#else
#  undef main // SDL_main is included via SDL.h
int main(int argc, char** argv)
#endif
{
	int errorCode = 0;

	try
	{
		SDL_setup(gScreenWidth, gScreenHeight);

		// Timing
		const int targetFreq = 60;
		const float updateTime = 1.0f / targetFreq;

		float lastTime = SDL_getTime();
		float accum = 0.0f;

		// Main loop
		while (true)
		{
			// Update time
			float curTime = SDL_getTime();
			float frameTime = curTime - lastTime;

			lastTime = curTime;
			accum += frameTime;

			// Process window messages
			if (!SDL_processEvents(keyboardHandler))
			{
				throw exception("Exiting");
			}

			// Update logic
			while (accum >= updateTime)
			{
				accum -= updateTime;

				if (moveForward)
				{
					gPlayer.move(100 * updateTime);
				}
				if (moveBackward)
				{
					gPlayer.move(-100 * updateTime);
				}
				if (turnLeft)
				{
					gPlayer.turn(-180 * updateTime);
				}
				if (turnRight)
				{
					gPlayer.turn(180 * updateTime);
				}
			}

			// Render
			SDL_render(render);
		}
	}
	catch (exception& e)
	{
		cout << e.what() << endl;
		errorCode = 1;
	}

	// Shutdown
	SDL_shutdown();
	return errorCode;
}