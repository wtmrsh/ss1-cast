// Platform
#include "Platform.h"

// Standard headers
#include <string>
#include <map>

// 3rd party headers
#include "sdl/SDL.h"

// User headers
#include "SDL.h"

using namespace std;

static SDL_Window* gWindow = nullptr;
static SDL_Renderer* gRenderer = nullptr;
static SDL_Texture* gScreenTexture = nullptr;

static unsigned char* gCurKeyBuffer = nullptr;
static unsigned char* gPrevKeyBuffer = nullptr;
static map<int, Key> gKeyTranslator;

static int gScreenWidth;
static int gScreenHeight;
static int gBytesPerPixel;

//
// Input
//
void saveKeys()
{
	memcpy(gPrevKeyBuffer, gCurKeyBuffer, (int)Key::NUMKEYS * sizeof(unsigned char));
}

void setKey(Key key, bool down)
{
	gCurKeyBuffer[(int)key] = down ? 1 : 0;
}

bool keyPressed(Key key)
{
	return gCurKeyBuffer[(int)key] == 1 && gPrevKeyBuffer[(int)key] == 0;
}

bool keyReleased(Key key)
{
	return gCurKeyBuffer[(int)key] == 0 && gPrevKeyBuffer[(int)key] == 1;
}

bool keyDown(Key key)
{
	return gCurKeyBuffer[(int)key] == 1 && gPrevKeyBuffer[(int)key] == 1;
}

KeyModifiers getKeyModifiers(unsigned short mod)
{
	unsigned int km = (int)KeyModifiers::None;

	if (mod & KMOD_LSHIFT)
		km += (int)KeyModifiers::LeftShift;
	if (mod & KMOD_RSHIFT)
		km += (int)KeyModifiers::RightShift;
	if (mod & KMOD_SHIFT)
		km += (int)KeyModifiers::Shift;
	if (mod & KMOD_LCTRL)
		km += (int)KeyModifiers::LeftCtrl;
	if (mod & KMOD_RCTRL)
		km += (int)KeyModifiers::RightCtrl;
	if (mod & KMOD_CTRL)
		km += (int)KeyModifiers::Ctrl;
	if (mod & KMOD_LALT)
		km += (int)KeyModifiers::LeftAlt;
	if (mod & KMOD_RALT)
		km += (int)KeyModifiers::RightAlt;
	if (mod & KMOD_ALT)
		km += (int)KeyModifiers::Alt;
	if (mod & KMOD_NUM)
		km += (int)KeyModifiers::NumLock;
	if (mod & KMOD_CAPS)
		km += (int)KeyModifiers::CapsLock;

	return (KeyModifiers)km;
}

//
// SDL graphics
//
void SDL_setup(int screenWidth, int screenHeight)
{
	gScreenWidth = screenWidth;
	gScreenHeight = screenHeight;
	gBytesPerPixel = 4;

	// Init video system
	if (SDL_Init(SDL_INIT_VIDEO) < 0)
	{
		throw exception("Could not initialise SDL.");
	}

	// Create window
	gWindow = SDL_CreateWindow(
		"ss1-raycast",
		SDL_WINDOWPOS_UNDEFINED,
		SDL_WINDOWPOS_UNDEFINED,
		gScreenWidth,
		gScreenHeight,
		SDL_WINDOW_SHOWN);

	if (!gWindow)
	{
		string errMsg = "Could not create SDL window: " + string(SDL_GetError());
		throw exception(errMsg.c_str());
	}

	// Create renderer
	gRenderer = SDL_CreateRenderer(gWindow, -1, SDL_RENDERER_ACCELERATED);

	if (!gRenderer)
	{
		string errMsg = "Could not create SDL renderer: " + string(SDL_GetError());
		throw exception(errMsg.c_str());
	}

	// Create screen texture
	gScreenTexture = SDL_CreateTexture(
		gRenderer,
		SDL_PIXELFORMAT_BGRA8888,
		SDL_TEXTUREACCESS_STREAMING,
		gScreenWidth,
		gScreenHeight);

	if (!gScreenTexture)
	{
		string errMsg = "Could not create SDL screen buffer: " + string(SDL_GetError());
		throw exception(errMsg.c_str());
	}

	// Setup input
	gCurKeyBuffer = new unsigned char[(int)Key::NUMKEYS];
	gPrevKeyBuffer = new unsigned char[(int)Key::NUMKEYS];

	for (int i = 0; i < (int)Key::NUMKEYS; ++i)
	{
		gCurKeyBuffer[i] = gPrevKeyBuffer[i] = 0;
	}

	gKeyTranslator[SDLK_ESCAPE] = Key::Escape;
	gKeyTranslator[SDLK_1] = Key::_1;
	gKeyTranslator[SDLK_2] = Key::_2;
	gKeyTranslator[SDLK_3] = Key::_3;
	gKeyTranslator[SDLK_4] = Key::_4;
	gKeyTranslator[SDLK_5] = Key::_5;
	gKeyTranslator[SDLK_6] = Key::_6;
	gKeyTranslator[SDLK_7] = Key::_7;
	gKeyTranslator[SDLK_8] = Key::_8;
	gKeyTranslator[SDLK_9] = Key::_9;
	gKeyTranslator[SDLK_0] = Key::_0;
	gKeyTranslator[SDLK_MINUS] = Key::Minus;
	gKeyTranslator[SDLK_EQUALS] = Key::Equals;
	gKeyTranslator[SDLK_BACKSPACE] = Key::Backspace;
	gKeyTranslator[SDLK_TAB] = Key::Tab;
	gKeyTranslator[SDLK_a] = Key::A;
	gKeyTranslator[SDLK_b] = Key::B;
	gKeyTranslator[SDLK_c] = Key::C;
	gKeyTranslator[SDLK_d] = Key::D;
	gKeyTranslator[SDLK_e] = Key::E;
	gKeyTranslator[SDLK_f] = Key::F;
	gKeyTranslator[SDLK_g] = Key::G;
	gKeyTranslator[SDLK_h] = Key::H;
	gKeyTranslator[SDLK_i] = Key::I;
	gKeyTranslator[SDLK_j] = Key::J;
	gKeyTranslator[SDLK_k] = Key::K;
	gKeyTranslator[SDLK_l] = Key::L;
	gKeyTranslator[SDLK_m] = Key::M;
	gKeyTranslator[SDLK_n] = Key::N;
	gKeyTranslator[SDLK_o] = Key::O;
	gKeyTranslator[SDLK_p] = Key::P;
	gKeyTranslator[SDLK_q] = Key::Q;
	gKeyTranslator[SDLK_r] = Key::R;
	gKeyTranslator[SDLK_s] = Key::S;
	gKeyTranslator[SDLK_t] = Key::T;
	gKeyTranslator[SDLK_u] = Key::U;
	gKeyTranslator[SDLK_v] = Key::V;
	gKeyTranslator[SDLK_w] = Key::W;
	gKeyTranslator[SDLK_x] = Key::X;
	gKeyTranslator[SDLK_y] = Key::Y;
	gKeyTranslator[SDLK_z] = Key::Z;

	gKeyTranslator[SDLK_LEFTBRACKET] = Key::LeftBracket;
	gKeyTranslator[SDLK_RIGHTBRACKET] = Key::RightBracket;
	gKeyTranslator[SDLK_RETURN] = Key::Enter;
	gKeyTranslator[SDLK_LCTRL] = Key::LeftControl;
	gKeyTranslator[SDLK_RCTRL] = Key::RightControl;
	gKeyTranslator[SDLK_SEMICOLON] = Key::Semicolon;
	gKeyTranslator[SDLK_AT] = Key::Apostrophe;
	gKeyTranslator[SDLK_BACKQUOTE] = Key::Tilde;
	gKeyTranslator[SDLK_LSHIFT] = Key::LeftShift;
	gKeyTranslator[SDLK_RSHIFT] = Key::RightShift;
	gKeyTranslator[SDLK_BACKSLASH] = Key::Backslash;
	gKeyTranslator[SDLK_COMMA] = Key::Comma;
	gKeyTranslator[SDLK_PERIOD] = Key::Period;
	gKeyTranslator[SDLK_SLASH] = Key::Slash;
	gKeyTranslator[SDLK_LALT] = Key::LeftAlt;
	gKeyTranslator[SDLK_RALT] = Key::RightAlt;
	gKeyTranslator[SDLK_SPACE] = Key::Space;
	gKeyTranslator[SDLK_CAPSLOCK] = Key::CapsLock;
	gKeyTranslator[SDLK_NUMLOCKCLEAR] = Key::NumLock;
	gKeyTranslator[SDLK_SCROLLLOCK] = Key::ScrollLock;
	gKeyTranslator[SDLK_F1] = Key::F1;
	gKeyTranslator[SDLK_F2] = Key::F2;
	gKeyTranslator[SDLK_F3] = Key::F3;
	gKeyTranslator[SDLK_F4] = Key::F4;
	gKeyTranslator[SDLK_F5] = Key::F5;
	gKeyTranslator[SDLK_F6] = Key::F6;
	gKeyTranslator[SDLK_F7] = Key::F7;
	gKeyTranslator[SDLK_F8] = Key::F8;
	gKeyTranslator[SDLK_F9] = Key::F9;
	gKeyTranslator[SDLK_F10] = Key::F10;
	gKeyTranslator[SDLK_F11] = Key::F11;
	gKeyTranslator[SDLK_F12] = Key::F12;
	gKeyTranslator[SDLK_KP_MULTIPLY] = Key::NumpadMultiply;
	gKeyTranslator[SDLK_KP_MINUS] = Key::NumpadMinus;
	gKeyTranslator[SDLK_KP_PLUS] = Key::NumpadPlus;
	gKeyTranslator[SDLK_KP_DIVIDE] = Key::NumpadDivide;
	gKeyTranslator[SDLK_KP_0] = Key::Numpad_0;
	gKeyTranslator[SDLK_KP_1] = Key::Numpad_1;
	gKeyTranslator[SDLK_KP_2] = Key::Numpad_2;
	gKeyTranslator[SDLK_KP_3] = Key::Numpad_3;
	gKeyTranslator[SDLK_KP_4] = Key::Numpad_4;
	gKeyTranslator[SDLK_KP_5] = Key::Numpad_5;
	gKeyTranslator[SDLK_KP_6] = Key::Numpad_6;
	gKeyTranslator[SDLK_KP_7] = Key::Numpad_7;
	gKeyTranslator[SDLK_KP_8] = Key::Numpad_8;
	gKeyTranslator[SDLK_KP_9] = Key::Numpad_9;
	gKeyTranslator[SDLK_KP_PERIOD] = Key::NumpadPeriod;
	gKeyTranslator[SDLK_KP_ENTER] = Key::NumpadEnter;
	gKeyTranslator[SDLK_PRINTSCREEN] = Key::PrintScreen;
	gKeyTranslator[SDLK_PAUSE] = Key::Pause;
	gKeyTranslator[SDLK_HOME] = Key::Home;
	gKeyTranslator[SDLK_END] = Key::End;
	gKeyTranslator[SDLK_UP] = Key::UpArrow;
	gKeyTranslator[SDLK_DOWN] = Key::DownArrow;
	gKeyTranslator[SDLK_LEFT] = Key::LeftArrow;
	gKeyTranslator[SDLK_RIGHT] = Key::RightArrow;
	gKeyTranslator[SDLK_PAGEUP] = Key::PageUp;
	gKeyTranslator[SDLK_PAGEDOWN] = Key::PageDown;
	gKeyTranslator[SDLK_INSERT] = Key::Insert;
	gKeyTranslator[SDLK_DELETE] = Key::Delete;
}

void SDL_shutdown()
{
	// Destroy input
	delete[] gCurKeyBuffer;
	gCurKeyBuffer = nullptr;

	delete[] gPrevKeyBuffer;
	gPrevKeyBuffer = nullptr;

	// Destroy screen texture
	if (gScreenTexture)
	{
		SDL_DestroyTexture(gScreenTexture);
		gScreenTexture = nullptr;
	}

	// Destroy renderer
	if (gRenderer)
	{
		SDL_DestroyRenderer(gRenderer);
		gRenderer = nullptr;
	}

	// Destroy window
	if (gWindow)
	{
		SDL_DestroyWindow(gWindow);
		gWindow = nullptr;
	}

	SDL_Quit();
}

void SDL_render(function<void(int*, int, int, int, int, int)> renderFn)
{
	// Lock texture
	int pitch = gScreenWidth * gBytesPerPixel;
	void* pixels;

	SDL_LockTexture(gScreenTexture, nullptr, &pixels, &pitch);

	// Update
	memset(pixels, 0, gScreenWidth * gScreenHeight * gBytesPerPixel);

	renderFn((int*)pixels, gScreenWidth, 0, 0, gScreenWidth, gScreenHeight);

	// Unlock and copy to screen texture
	SDL_UnlockTexture(gScreenTexture);
	SDL_RenderCopy(gRenderer, gScreenTexture, nullptr, nullptr);
	SDL_RenderPresent(gRenderer);
}

//
// SDL events
//
bool SDL_processEvents(function<void(KeyEvent, Key, KeyModifiers)> keyboardHandler)
{
	saveKeys();

	Key key;

	SDL_Event evt;
	while (SDL_PollEvent(&evt))
	{
		switch (evt.type)
		{
		case SDL_KEYDOWN:
			key = gKeyTranslator[evt.key.keysym.sym];
			setKey(key, true);

			if (keyPressed(key))
			{
				auto keyEventType = KeyEvent::Pressed;
				auto keyModifiers = getKeyModifiers(evt.key.keysym.mod);
				keyboardHandler(keyEventType, key, keyModifiers);
			}
			break;

		case SDL_KEYUP:
			key = gKeyTranslator[evt.key.keysym.sym];
			setKey(key, false);

			if (keyReleased(key))
			{
				auto keyEventType = KeyEvent::Released;
				auto keyModifiers = getKeyModifiers(evt.key.keysym.mod);
				keyboardHandler(keyEventType, key, keyModifiers);
			}
			break;

		case SDL_QUIT:
			return false;

		default:
			break;
		}
	}

	switch (evt.window.event)
	{
	case SDL_WINDOWEVENT_CLOSE:
		return false;

	default:
		break;
	}

	return true;
}

//
// SDL timing
//
float SDL_getTime()
{
	return SDL_GetTicks() / 1000.0f;
}

