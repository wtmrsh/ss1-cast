#include "Intersection.h"

bool Intersection::rayLine(Vector2 const& origin, Vector2 const& dir, Vector2 const& line0, Vector2 const& line1, Vector2* hitPos, float* hitDist)
{
	Vector2 s0 = origin - line0;
	Vector2 s1 = line1 - line0;
	Vector2 s2(-dir.y, dir.x);

	float dp = s1.dot(s2);
	if (fabs(dp) < 0.0001f)
	{
		return false;
	}

	float t1 = (s1.x * s0.y - s1.y - s0.x) / dp;
	float t2 = s0.dot(s2) / dp;

	if (t1 >= 0.0f && (t2 >= 0.0f && t2 <= 1.0f))
	{
		if (hitPos)
		{
			*hitPos = origin + dir * t1;
			*hitDist = t1;
		}

		return true;
	}

	return false;
}